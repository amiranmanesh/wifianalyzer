/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer.about

import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.vrem.util.FileUtils
import com.vrem.wifianalyzer.BuildConfig
import com.vrem.wifianalyzer.MainContext
import com.vrem.wifianalyzer.R
import com.vrem.wifianalyzer.databinding.AboutContentBinding
import java.text.SimpleDateFormat
import java.util.*

class AboutFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = AboutContentBinding.inflate(inflater, container, false)
        setTexts(binding)
        setOnClicks(binding)
        return binding.root
    }

    private fun setTexts(binding: AboutContentBinding) {
        binding.aboutCopyright.text = copyright()
        binding.aboutVersionInfo.text = version()
        binding.aboutPackageName.text = BuildConfig.APPLICATION_ID
    }

    private fun setOnClicks(binding: AboutContentBinding) {
        val gpl = AlertDialogClickListener(activity!!, R.string.gpl, R.raw.gpl)
        binding.license.setOnClickListener(gpl)
        val contributors = AlertDialogClickListener(activity!!, R.string.about_contributor_title, R.raw.contributors, false)
        binding.contributors.setOnClickListener(contributors)
        val al = AlertDialogClickListener(activity!!, R.string.al, R.raw.al)
        binding.apacheCommonsLicense.setOnClickListener(al)
        binding.graphViewLicense.setOnClickListener(al)
        binding.materialDesignIconsLicense.setOnClickListener(al)
        binding.writeReview.setOnClickListener(WriteReviewClickListener(activity!!))
    }

    private fun copyright(): String {
        val locale = Locale.getDefault()
        val year = SimpleDateFormat(YEAR_FORMAT, locale).format(Date())
        val message = resources.getString(R.string.app_copyright)
        return message + year
    }

    private fun version(): String {
        var text = BuildConfig.VERSION_NAME + " - " + BuildConfig.VERSION_CODE
        val configuration = MainContext.INSTANCE.configuration
        if (configuration != null) {
            if (configuration.isSizeAvailable) {
                text += "S"
            }
            if (configuration.isLargeScreen) {
                text += "L"
            }
        }
        text += " (" + Build.VERSION.RELEASE + "-" + Build.VERSION.SDK_INT + ")"
        return text
    }

    private class WriteReviewClickListener(private val activity: Activity) : View.OnClickListener {
        override fun onClick(view: View) {
            val url = "market://details?id=" + BuildConfig.APPLICATION_ID
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            try {
                activity.startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(view.context, e.localizedMessage, Toast.LENGTH_LONG).show()
            }
        }

    }

    private class AlertDialogClickListener @JvmOverloads internal constructor(private val activity: Activity, private val titleId: Int, private val resourceId: Int, private val isSmallFont: Boolean = true) : View.OnClickListener {
        override fun onClick(view: View) {
            if (!activity.isFinishing) {
                val text = FileUtils.readFile(activity.resources, resourceId)
                val alertDialog = AlertDialog.Builder(view.context)
                        .setTitle(titleId)
                        .setMessage(text)
                        .setNeutralButton(android.R.string.ok, Close())
                        .create()
                alertDialog.show()
                if (isSmallFont) {
                    val textView = alertDialog.findViewById<TextView>(android.R.id.message)
                    textView.textSize = 8f
                }
            }
        }

        private class Close : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface, which: Int) {
                dialog.dismiss()
            }
        }

    }

    companion object {
        private const val YEAR_FORMAT = "yyyy"
    }
}