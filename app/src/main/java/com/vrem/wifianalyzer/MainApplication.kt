package com.vrem.wifianalyzer

import androidx.multidex.MultiDexApplication
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import ir.malv.utils.Pulp


class MainApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/VazirFD.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )

        Pulp.init(this).setMainTag("WifiAnalyzer")

//        Thread.setDefaultUncaughtExceptionHandler { t, e ->
//            Pulp.wtf(
//                TAG,
//                "** Uncaught Thread exception occurred **\n Thread: ${t.name}\n Cause: ${e.message}",
//                e
//            )
//        }
//        RxJavaPlugins.setErrorHandler { throwable ->
//            Pulp.wtf(
//                TAG,
//                "** Uncaught RxJavaPlugins exception occurred **\n Cause: ${throwable.message}",
//                throwable
//            )
//        }

    }

    companion object {
        const val TAG = "Application"
    }
}