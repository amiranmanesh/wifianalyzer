/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.vrem.util.ConfigurationUtils
import com.vrem.util.EnumUtils
import com.vrem.wifianalyzer.navigation.NavigationMenu
import com.vrem.wifianalyzer.navigation.NavigationMenuControl
import com.vrem.wifianalyzer.navigation.NavigationMenuController
import com.vrem.wifianalyzer.navigation.options.OptionMenu
import com.vrem.wifianalyzer.permission.PermissionService
import com.vrem.wifianalyzer.settings.Repository
import com.vrem.wifianalyzer.settings.SettingsFactory
import com.vrem.wifianalyzer.wifi.accesspoint.ConnectionView
import com.vrem.wifianalyzer.wifi.band.WiFiBand

class MainActivity : AppCompatActivity(), NavigationMenuControl, OnSharedPreferenceChangeListener {
    private var mainReload: MainReload? = null
    private var drawerNavigation: DrawerNavigation? = null
    var navigationMenuController: NavigationMenuController? = null
    var optionMenu: OptionMenu? = null
        private set
    private var currentCountryCode: String? = null
    var permissionService: PermissionService? = null
    override fun attachBaseContext(newBase: Context) {
        val repository = Repository(newBase)
        val settings = SettingsFactory.make(repository)
        val newLocale = settings.languageLocale
        val context = ConfigurationUtils.createContext(newBase, newLocale)
        super.attachBaseContext(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val mainContext = MainContext.INSTANCE
        mainContext.initialize(this, isLargeScreen)
        val settings = mainContext.settings
        settings.initializeDefaultValues()
        setTheme(settings.themeStyle.themeNoActionBar)
        setWiFiChannelPairs(mainContext)
        mainReload = MainReload(settings)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        settings.registerOnSharedPreferenceChangeListener(this)
        setOptionMenu(OptionMenu())
        ActivityUtils.keepScreenOn()
        val toolbar = ActivityUtils.setupToolbar()
        drawerNavigation = DrawerNavigation(this, toolbar)
        navigationMenuController = NavigationMenuController(this)
        navigationMenuController!!.currentNavigationMenu = settings.selectedMenu
        onNavigationItemSelected(currentMenuItem)
        val connectionView = ConnectionView(this)
        mainContext.scannerService.register(connectionView)
        permissionService = PermissionService(this)
        permissionService!!.check()
    }

    public override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerNavigation!!.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerNavigation!!.onConfigurationChanged(newConfig)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (!permissionService!!.isGranted(requestCode, grantResults)) {
            finish()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun setWiFiChannelPairs(mainContext: MainContext) {
        val settings = mainContext.settings
        val countryCode = settings.countryCode
        if (countryCode != currentCountryCode) {
            val pair = WiFiBand.GHZ5.wiFiChannels.getWiFiChannelPairFirst(countryCode)
            mainContext.configuration.wiFiChannelPair = pair
            currentCountryCode = countryCode
        }
    }

    private val isLargeScreen: Boolean
        private get() {
            val configuration = resources.configuration
            val screenLayoutSize = configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK
            return screenLayoutSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
                    screenLayoutSize == Configuration.SCREENLAYOUT_SIZE_XLARGE
        }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        val mainContext = MainContext.INSTANCE
        if (mainReload!!.shouldReload(mainContext.settings)) {
            MainContext.INSTANCE.scannerService.stop()
            recreate()
        } else {
            ActivityUtils.keepScreenOn()
            setWiFiChannelPairs(mainContext)
            update()
        }
    }

    fun update() {
        MainContext.INSTANCE.scannerService.update()
        updateActionBar()
    }

    override fun onBackPressed() {
        if (!closeDrawer()) {
            val selectedMenu = MainContext.INSTANCE.settings.selectedMenu
            if (selectedMenu == currentNavigationMenu) {
                super.onBackPressed()
            } else {
                currentNavigationMenu = selectedMenu
                onNavigationItemSelected(currentMenuItem)
            }
        }
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        closeDrawer()
        val currentNavigationMenu = EnumUtils.find(NavigationMenu::class.java, menuItem.itemId, NavigationMenu.ACCESS_POINTS)
        currentNavigationMenu.activateNavigationMenu(this, menuItem)
        return true
    }

    private fun closeDrawer(): Boolean {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
            return true
        }
        return false
    }

    public override fun onPause() {
        MainContext.INSTANCE.scannerService.pause()
        updateActionBar()
        super.onPause()
    }

    public override fun onResume() {
        super.onResume()
        if (permissionService!!.isPermissionGranted) {
            if (!permissionService!!.isSystemEnabled) {
                ActivityUtils.startLocationSettings()
            }
            MainContext.INSTANCE.scannerService.resume()
        }
        updateActionBar()
    }

    public override fun onStop() {
        MainContext.INSTANCE.scannerService.stop()
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        optionMenu!!.create(this, menu)
        updateActionBar()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        optionMenu!!.select(item)
        updateActionBar()
        return true
    }

    fun updateActionBar() {
        currentNavigationMenu.activateOptions(this)
    }

    fun setOptionMenu(optionMenu: OptionMenu) {
        this.optionMenu = optionMenu
    }

    override fun getCurrentMenuItem(): MenuItem {
        return navigationMenuController!!.currentMenuItem
    }

    override fun getCurrentNavigationMenu(): NavigationMenu {
        return navigationMenuController!!.currentNavigationMenu
    }

    override fun setCurrentNavigationMenu(navigationMenu: NavigationMenu) {
        navigationMenuController!!.currentNavigationMenu = navigationMenu
        MainContext.INSTANCE.settings.saveSelectedMenu(navigationMenu)
    }

    override fun getNavigationView(): NavigationView {
        return navigationMenuController!!.navigationView
    }

    fun mainConnectionVisibility(visibility: Int) {
        findViewById<View>(R.id.main_connection).visibility = visibility
    }

    fun setDrawerNavigation(drawerNavigation: DrawerNavigation?) {
        this.drawerNavigation = drawerNavigation
    }

}