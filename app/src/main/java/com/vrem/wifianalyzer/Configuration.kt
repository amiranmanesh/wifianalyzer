/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer

import androidx.core.util.Pair
import com.vrem.wifianalyzer.wifi.band.WiFiChannel
import com.vrem.wifianalyzer.wifi.band.WiFiChannels

class Configuration(val isLargeScreen: Boolean) {
    private var size = 0
    lateinit var wiFiChannelPair: Pair<WiFiChannel, WiFiChannel>

    val isSizeAvailable: Boolean
        get() = size == SIZE_MAX

    fun setSize(size: Int) {
        this.size = size
    }

    companion object {
        const val SIZE_MIN = 1024
        const val SIZE_MAX = 4096
    }

    init {
        setSize(SIZE_MAX)
        wiFiChannelPair = WiFiChannels.UNKNOWN
    }
}