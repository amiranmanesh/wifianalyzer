/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer.permission

import android.app.Activity

class PermissionService(activity: Activity) {
    private var systemPermission: SystemPermission
    private var applicationPermission: ApplicationPermission
    val isEnabled: Boolean
        get() = isSystemEnabled && isPermissionGranted

    val isSystemEnabled: Boolean
        get() = systemPermission.isEnabled

    fun check() {
        applicationPermission.check()
    }

    fun isGranted(requestCode: Int, grantResults: IntArray?): Boolean {
        return applicationPermission.isGranted(requestCode, grantResults!!)
    }

    val isPermissionGranted: Boolean
        get() = applicationPermission.isGranted

    fun setSystemPermission(systemPermission: SystemPermission) {
        this.systemPermission = systemPermission
    }

    fun setApplicationPermission(applicationPermission: ApplicationPermission) {
        this.applicationPermission = applicationPermission
    }

    init {
        applicationPermission = ApplicationPermission(activity)
        systemPermission = SystemPermission(activity)
    }
}