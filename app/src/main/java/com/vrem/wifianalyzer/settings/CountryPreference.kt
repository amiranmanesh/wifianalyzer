/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer.settings

import android.content.Context
import android.util.AttributeSet
import com.vrem.util.LocaleUtils
import com.vrem.wifianalyzer.MainContext
import com.vrem.wifianalyzer.wifi.band.WiFiChannelCountry
import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.collections4.Transformer
import java.util.*

class CountryPreference(context: Context, attrs: AttributeSet?) : CustomPreference(context, attrs, data, LocaleUtils.getDefaultCountryCode()) {
    private class ToData() : Transformer<WiFiChannelCountry, Data> {
        private val currentLocale: Locale
        private val locale: Locale
            get() {
                val settings = MainContext.INSTANCE.settings
                return settings.languageLocale
            }

        override fun transform(input: WiFiChannelCountry): Data {
            return Data(input.countryCode, input.getCountryName(currentLocale))
        }

        init {
            currentLocale = locale
        }
    }

    companion object {
        private val data: List<Data>
            get() {
                val results: List<Data> = ArrayList(CollectionUtils.collect(WiFiChannelCountry.getAll(), ToData()))
                Collections.sort(results)
                return results
            }
    }
}