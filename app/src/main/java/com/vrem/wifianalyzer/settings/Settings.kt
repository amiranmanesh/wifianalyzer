/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer.settings

import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import com.vrem.util.EnumUtils
import com.vrem.util.LocaleUtils
import com.vrem.wifianalyzer.R
import com.vrem.wifianalyzer.navigation.NavigationGroup
import com.vrem.wifianalyzer.navigation.NavigationMenu
import com.vrem.wifianalyzer.wifi.accesspoint.AccessPointViewType
import com.vrem.wifianalyzer.wifi.accesspoint.ConnectionViewType
import com.vrem.wifianalyzer.wifi.band.WiFiBand
import com.vrem.wifianalyzer.wifi.graphutils.GraphLegend
import com.vrem.wifianalyzer.wifi.model.GroupBy
import com.vrem.wifianalyzer.wifi.model.Security
import com.vrem.wifianalyzer.wifi.model.SortBy
import com.vrem.wifianalyzer.wifi.model.Strength
import java.util.*

open class Settings internal constructor(val repository: Repository) {
    fun initializeDefaultValues() {
        repository.initializeDefaultValues()
    }

    fun registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener: OnSharedPreferenceChangeListener?) {
        repository.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener)
    }

    open val scanSpeed: Int
        get() {
            val defaultValue = repository.getStringAsInteger(R.string.scan_speed_default, SCAN_SPEED_DEFAULT)
            return repository.getStringAsInteger(R.string.scan_speed_key, defaultValue)
        }

    open val isWiFiThrottleDisabled: Boolean
        get() = false

    val graphMaximumY: Int
        get() {
            val defaultValue = repository.getStringAsInteger(R.string.graph_maximum_y_default, GRAPH_Y_DEFAULT)
            val result = repository.getStringAsInteger(R.string.graph_maximum_y_key, defaultValue)
            return result * GRAPH_Y_MULTIPLIER
        }

    fun toggleWiFiBand() {
        repository.save(R.string.wifi_band_key, wiFiBand.toggle().ordinal)
    }

    val countryCode: String
        get() {
            val countryCode = LocaleUtils.getDefaultCountryCode()
            return repository.getString(R.string.country_code_key, countryCode)
        }

    val languageLocale: Locale
        get() {
            val defaultLanguageTag = LocaleUtils.getDefaultLanguageTag()
            val languageTag = repository.getString(R.string.language_key, defaultLanguageTag)
            return LocaleUtils.findByLanguageTag(languageTag)
        }

    val sortBy: SortBy
        get() = find(SortBy::class.java, R.string.sort_by_key, SortBy.STRENGTH)

    val groupBy: GroupBy
        get() = find(GroupBy::class.java, R.string.group_by_key, GroupBy.NONE)

    val accessPointView: AccessPointViewType
        get() = find(AccessPointViewType::class.java, R.string.ap_view_key, AccessPointViewType.COMPLETE)

    val connectionViewType: ConnectionViewType
        get() = find(ConnectionViewType::class.java, R.string.connection_view_key, ConnectionViewType.COMPLETE)

    val channelGraphLegend: GraphLegend
        get() = find(GraphLegend::class.java, R.string.channel_graph_legend_key, GraphLegend.HIDE)

    val timeGraphLegend: GraphLegend
        get() = find(GraphLegend::class.java, R.string.time_graph_legend_key, GraphLegend.LEFT)

    val wiFiBand: WiFiBand
        get() = find(WiFiBand::class.java, R.string.wifi_band_key, WiFiBand.GHZ2)

    open val isWiFiOffOnExit: Boolean
        get() = repository.getBoolean(R.string.wifi_off_on_exit_key, repository.getResourceBoolean(R.bool.wifi_off_on_exit_default))

    val isKeepScreenOn: Boolean
        get() = repository.getBoolean(R.string.keep_screen_on_key, repository.getResourceBoolean(R.bool.keep_screen_on_default))

    val themeStyle: ThemeStyle
        get() = find(ThemeStyle::class.java, R.string.theme_key, ThemeStyle.DARK)

    val selectedMenu: NavigationMenu
        get() = find(NavigationMenu::class.java, R.string.selected_menu_key, NavigationMenu.ACCESS_POINTS)

    fun saveSelectedMenu(navigationMenu: NavigationMenu) {
        if (NavigationGroup.GROUP_FEATURE.navigationMenus.contains(navigationMenu)) {
            repository.save(R.string.selected_menu_key, navigationMenu.ordinal)
        }
    }

    val sSIDs: Set<String>
        get() = repository.getStringSet(R.string.filter_ssid_key, HashSet())

    fun saveSSIDs(values: Set<String?>) {
        repository.saveStringSet(R.string.filter_ssid_key, values as Set<String>)
    }

    val wiFiBands: Set<WiFiBand>
        get() = findSet(WiFiBand::class.java, R.string.filter_wifi_band_key, WiFiBand.GHZ2)

    fun saveWiFiBands(values: Set<WiFiBand>) {
        saveSet(R.string.filter_wifi_band_key, values)
    }

    val strengths: Set<Strength>
        get() = findSet(Strength::class.java, R.string.filter_strength_key, Strength.FOUR)

    fun saveStrengths(values: Set<Strength>) {
        saveSet(R.string.filter_strength_key, values)
    }

    val securities: Set<Security>
        get() = findSet(Security::class.java, R.string.filter_security_key, Security.NONE)

    fun saveSecurities(values: Set<Security>) {
        saveSet(R.string.filter_security_key, values)
    }

    private fun <T : Enum<*>?> find(enumType: Class<T>, key: Int, defaultValue: T): T {
        val value = repository.getStringAsInteger(key, defaultValue!!.ordinal)
        return EnumUtils.find(enumType, value, defaultValue)
    }

    private fun <T : Enum<*>?> findSet(enumType: Class<T>, key: Int, defaultValue: T): Set<T> {
        val defaultValues = EnumUtils.ordinals(enumType)
        val values = repository.getStringSet(key, defaultValues)
        return EnumUtils.find(enumType, values, defaultValue)
    }

    private fun <T : Enum<*>?> saveSet(key: Int, values: Set<T>) {
        repository.saveStringSet(key, EnumUtils.find(values))
    }

    companion object {
        const val SCAN_SPEED_DEFAULT = 5
        const val GRAPH_Y_MULTIPLIER = -10
        const val GRAPH_Y_DEFAULT = 2
    }

}