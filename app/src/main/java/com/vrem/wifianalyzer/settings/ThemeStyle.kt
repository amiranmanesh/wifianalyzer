/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer.settings

import androidx.annotation.StyleRes
import com.vrem.wifianalyzer.MainContext
import com.vrem.wifianalyzer.R

enum class ThemeStyle(@get:StyleRes
                      @param:StyleRes val theme: Int, @get:StyleRes
                      @param:StyleRes val themeNoActionBar: Int) {
    DARK(R.style.ThemeDark, R.style.ThemeDarkNoActionBar), LIGHT(R.style.ThemeLight, R.style.ThemeLightNoActionBar), SYSTEM(R.style.ThemeSystem, R.style.ThemeSystemNoActionBar);

    companion object {
        @JvmStatic
        @get:StyleRes
        val defaultTheme: Int
            get() {
                val settings = MainContext.INSTANCE.settings
                val themeStyle = settings.themeStyle ?: DARK
                return themeStyle.theme
            }
    }

}