/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer

import android.annotation.TargetApi
import android.os.Build
import android.provider.Settings
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import com.vrem.util.IntentUtils

class ActivityUtils private constructor() {
    internal class WiFiBandToggle(private val mainActivity: MainActivity) : View.OnClickListener {
        override fun onClick(view: View) {
            if (mainActivity.currentNavigationMenu.isWiFiBandSwitchable) {
                MainContext.INSTANCE.settings.toggleWiFiBand()
            }
        }

    }

    companion object {
        @JvmStatic
        fun setActionBarOptions(actionBar: ActionBar?) {
            if (actionBar == null) {
                return
            }
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        @JvmStatic
        fun keepScreenOn() {
            val mainContext = MainContext.INSTANCE
            val settings = mainContext.settings ?: return
            val mainActivity = mainContext.mainActivity
            val window = mainActivity.window
            if (settings.isKeepScreenOn) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            } else {
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
        }

        @JvmStatic
        fun setupToolbar(): Toolbar {
            val mainActivity = MainContext.INSTANCE.mainActivity
            val toolbar = mainActivity.findViewById<Toolbar>(R.id.toolbar)
            toolbar.setOnClickListener(WiFiBandToggle(mainActivity))
            mainActivity.setSupportActionBar(toolbar)
            setActionBarOptions(mainActivity.supportActionBar)
            return toolbar
        }

        @JvmStatic
        @TargetApi(Build.VERSION_CODES.Q)
        fun startWiFiSettings() {
            val mainActivity = MainContext.INSTANCE.mainActivity
            val intent = IntentUtils.makeIntent(Settings.Panel.ACTION_WIFI)
            mainActivity.startActivityForResult(intent, 0)
        }

        @JvmStatic
        fun startLocationSettings() {
            val mainActivity = MainContext.INSTANCE.mainActivity
            val intent = IntentUtils.makeIntent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            mainActivity.startActivity(intent)
        }
    }

    init {
        throw IllegalStateException("Utility class")
    }
}