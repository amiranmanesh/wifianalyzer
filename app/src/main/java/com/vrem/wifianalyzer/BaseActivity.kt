package com.vrem.wifianalyzer

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import ir.malv.utils.Pulp


abstract class BaseActivity : AppCompatActivity() {

    lateinit var fragmentManager: FragmentManager

    /**
     * To use and configure font
     * Calligraphy used to configure font.
     */
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onStart() {
        super.onStart()
        log(this.localClassName, "onStart")
    }

    override fun onStop() {
        super.onStop()
        log(this.localClassName, "onStop")
    }

    override fun onRestart() {
        super.onRestart()
        log(this.localClassName, "onRestart")
    }

    override fun onResume() {
        super.onResume()
        log(this.localClassName, "onResume")
    }

    override fun onPause() {
        super.onPause()
        log(this.localClassName, "onPause")
    }

    override fun onDestroy() {
        super.onDestroy()
        log(this.localClassName, "onDestroy")
    }
    //End

    @SuppressLint("ObsoleteSdkInt", "SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Set orientation to Portrait
        log(this.localClassName, "onCreate")

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
        }
        supportActionBar?.hide()


        fragmentManager = supportFragmentManager
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun toast(message: String) {
        try {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        } catch (e: java.lang.Exception) {
            Pulp.wtf(TAG, "error in toast", e)
        }
    }

    /**
     * Show a simple toast but very faster.
     * @param message is the message that will be shown.
     */
    protected fun toastShort(message: String) {
        try {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        } catch (e: java.lang.Exception) {
            Pulp.wtf(TAG, "error in toast", e)
        }
    }

    /**
     * To log an event to analytics.
     */
    private fun log(screenName: String, event: String) {
        Pulp.info(TAG, "Submitting Event") {
            "Screen" to screenName
            "Event" to event
        }
    }


    companion object {
        const val TAG = "Base activity"
    }
}