/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer.vendor.model

import org.apache.commons.lang3.StringUtils
import java.util.*

internal class VendorUtils private constructor() {
    companion object {
        const val MAX_SIZE = 6
        private const val SEPARATOR = ":"

        @JvmStatic
        fun clean(macAddress: String?): String {
            if (macAddress == null) {
                return StringUtils.EMPTY
            }
            val result = macAddress.replace(SEPARATOR, "")
            val locale = Locale.getDefault()
            return result.substring(0, result.length.coerceAtMost(MAX_SIZE)).toUpperCase(locale)
        }

        @JvmStatic
        fun toMacAddress(source: String?): String {
            if (source == null) {
                return StringUtils.EMPTY
            }
            return if (source.length < MAX_SIZE) {
                "*$source*"
            } else "${source.substring(0, 2)}${SEPARATOR}${source.substring(2, 4)}${SEPARATOR}${source.substring(4, 6)}"
        }
    }

    init {
        throw IllegalStateException("Utility class")
    }
}