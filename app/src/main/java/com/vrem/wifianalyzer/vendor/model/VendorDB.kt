/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer.vendor.model

import android.content.res.Resources
import com.vrem.util.FileUtils
import com.vrem.wifianalyzer.R
import org.apache.commons.collections4.CollectionUtils
import org.apache.commons.collections4.IterableUtils
import org.apache.commons.collections4.Predicate
import org.apache.commons.collections4.PredicateUtils
import org.apache.commons.lang3.StringUtils
import java.util.*

internal class VendorDB(private val resources: Resources) : VendorService {
    private val vendors: MutableMap<String, List<String>>
    private val macs: MutableMap<String, String>
    private var loaded: Boolean
    override fun findVendorName(macAddress: String?): String {
        val result = getMacs()[VendorUtils.clean(macAddress!!)]
        return result ?: StringUtils.EMPTY
    }

    override fun findMacAddresses(vendorName: String?): List<String?> {
        if (StringUtils.isBlank(vendorName)) {
            return ArrayList()
        }
        val locale = Locale.getDefault()
        val results = getVendors()[vendorName!!.toUpperCase(locale)]
        return results ?: ArrayList()
    }

    override fun findVendors(): List<String?> {
        return findVendors(StringUtils.EMPTY)
    }

    override fun findVendors(filter: String?): List<String?> {
        if (StringUtils.isBlank(filter)) {
            return ArrayList(getVendors().keys)
        }
        val locale = Locale.getDefault()
        val filterToUpperCase = filter!!.toUpperCase(locale)
        val predicates = listOf(StringContains(filterToUpperCase), MacContains(filterToUpperCase))
        return ArrayList(CollectionUtils.select(getVendors().keys, PredicateUtils.anyPredicate(predicates) as Nothing?))
    }

    fun getVendors(): Map<String, List<String>> {
        load(resources)
        return vendors
    }

    fun getMacs(): Map<String, String> {
        load(resources)
        return macs
    }

    private fun load(resources: Resources) {
        if (!loaded) {
            loaded = true
            val lines = FileUtils.readFile(resources, R.raw.data).split("\n").toTypedArray()
            for (data in lines) {
                if (data != null) {
                    val parts = data.split("\\|").toTypedArray()
                    if (parts.size == 2) {
                        val addresses: MutableList<String> = ArrayList()
                        val name = parts[0]
                        vendors[name] = addresses
                        val length = parts[1].length
                        var i = 0
                        while (i < length) {
                            val mac = parts[1].substring(i, i + VendorUtils.MAX_SIZE)
                            addresses.add(VendorUtils.toMacAddress(mac))
                            macs[mac] = name
                            i += VendorUtils.MAX_SIZE
                        }
                    }
                }
            }
        }
    }

    inner class StringContains(private val filter: String) : Predicate<String?> {
        override fun evaluate(`object`: String?): Boolean {
            return `object`!!.contains(filter)
        }

    }

    private inner class MacContains(private val filter: String) : Predicate<String> {
        override fun evaluate(`object`: String): Boolean {
            return IterableUtils.matchesAny(findMacAddresses(`object`), StringContains(this.filter))
        }

    }

    init {
        vendors = TreeMap()
        macs = TreeMap()
        loaded = false
    }
}