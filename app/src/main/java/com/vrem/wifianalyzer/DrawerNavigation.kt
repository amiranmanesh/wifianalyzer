/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer

import android.content.res.Configuration
import androidx.annotation.StringRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout

open class DrawerNavigation(mainActivity: MainActivity, toolbar: Toolbar) {
    private val drawerToggle: ActionBarDrawerToggle
    fun syncState() {
        drawerToggle.syncState()
    }

    fun onConfigurationChanged(newConfig: Configuration?) {
        drawerToggle.onConfigurationChanged(newConfig)
    }

    open fun create(
            mainActivity: MainActivity,
            toolbar: Toolbar,
            drawer: DrawerLayout,
            @StringRes openDrawerContentDescRes: Int,
            @StringRes closeDrawerContentDescRes: Int): ActionBarDrawerToggle {
        return ActionBarDrawerToggle(
                mainActivity,
                drawer,
                toolbar,
                openDrawerContentDescRes,
                closeDrawerContentDescRes)
    }

    init {
        val drawer = mainActivity.findViewById<DrawerLayout>(R.id.drawer_layout)
        drawerToggle = create(
                mainActivity,
                toolbar,
                drawer,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)
        drawer.addDrawerListener(drawerToggle)
        syncState()
    }
}