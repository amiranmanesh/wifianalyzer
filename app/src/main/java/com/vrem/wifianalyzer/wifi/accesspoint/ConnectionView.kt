/*
 * WiFiAnalyzer
 * Copyright (C) 2019  VREM Software Development <VREMSoftwareDevelopment@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.vrem.wifianalyzer.wifi.accesspoint

import android.net.wifi.WifiInfo
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.vrem.util.BuildUtils
import com.vrem.wifianalyzer.MainActivity
import com.vrem.wifianalyzer.MainContext
import com.vrem.wifianalyzer.R
import com.vrem.wifianalyzer.wifi.model.WiFiConnection
import com.vrem.wifianalyzer.wifi.model.WiFiData
import com.vrem.wifianalyzer.wifi.model.WiFiDetail
import com.vrem.wifianalyzer.wifi.scanner.UpdateNotifier
import java.util.*

class ConnectionView(private val mainActivity: MainActivity) : UpdateNotifier {
    private var accessPointDetail: AccessPointDetail? = null
    private var accessPointPopup: AccessPointPopup? = null
    override fun update(wiFiData: WiFiData) {
        val connectionViewType = MainContext.INSTANCE.settings.connectionViewType
        displayConnection(wiFiData, connectionViewType)
        displayNoData(wiFiData)
    }

    fun setAccessPointDetail(accessPointDetail: AccessPointDetail) {
        this.accessPointDetail = accessPointDetail
    }

    fun setAccessPointPopup(accessPointPopup: AccessPointPopup) {
        this.accessPointPopup = accessPointPopup
    }

    private fun displayNoData(wiFiData: WiFiData) {
        val visibility = if (noData(wiFiData)) View.VISIBLE else View.GONE
        mainActivity.findViewById<View>(R.id.scanning).visibility = visibility
        mainActivity.findViewById<View>(R.id.no_data).visibility = visibility
        if (BuildUtils.isMinVersionM()) {
            mainActivity.findViewById<View>(R.id.no_location).visibility = getNoLocationVisibility(visibility)
        }
    }

    private fun getNoLocationVisibility(visibility: Int): Int {
        return if (mainActivity.permissionService!!.isEnabled) View.GONE else visibility
    }

    private fun noData(wiFiData: WiFiData): Boolean {
        return mainActivity.currentNavigationMenu.isRegistered && wiFiData.wiFiDetails.isEmpty()
    }

    private fun displayConnection(wiFiData: WiFiData, connectionViewType: ConnectionViewType) {
        val connection = wiFiData.connection
        val connectionView = mainActivity.findViewById<View>(R.id.connection)
        val wiFiConnection = connection.wiFiAdditional.wiFiConnection
        if (connectionViewType.isHide || !wiFiConnection.isConnected) {
            connectionView.visibility = View.GONE
        } else {
            connectionView.visibility = View.VISIBLE
            val parent = connectionView.findViewById<ViewGroup>(R.id.connectionDetail)
            val view = accessPointDetail!!.makeView(parent.getChildAt(0), parent, connection, false, connectionViewType.accessPointViewType)
            if (parent.childCount == 0) {
                parent.addView(view)
            }
            setViewConnection(connectionView, wiFiConnection)
            attachPopup(view, connection)
        }
    }

    private fun setViewConnection(connectionView: View, wiFiConnection: WiFiConnection) {
        val ipAddress = wiFiConnection.ipAddress
        connectionView.findViewById<TextView>(R.id.ipAddress).text = ipAddress
        val textLinkSpeed = connectionView.findViewById<TextView>(R.id.linkSpeed)
        val linkSpeed = wiFiConnection.linkSpeed
        if (linkSpeed == WiFiConnection.LINK_SPEED_INVALID) {
            textLinkSpeed.visibility = View.GONE
        } else {
            textLinkSpeed.visibility = View.VISIBLE
            textLinkSpeed.text = String.format(Locale.ENGLISH, "%d%s", linkSpeed, WifiInfo.LINK_SPEED_UNITS)
        }
    }

    private fun attachPopup(view: View, wiFiDetail: WiFiDetail) {
        val popupView = view.findViewById<View>(R.id.attachPopup)
        if (popupView != null) {
            accessPointPopup!!.attach(popupView, wiFiDetail)
            accessPointPopup!!.attach(view.findViewById(R.id.ssid), wiFiDetail)
        }
    }

    init {
        setAccessPointDetail(AccessPointDetail())
        setAccessPointPopup(AccessPointPopup())
    }
}